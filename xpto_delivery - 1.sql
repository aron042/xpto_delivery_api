CREATE DATABASE IF NOT EXISTS `xpto_delivery`;
USE `xpto_delivery`;

CREATE TABLE points (
	`id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`source` VARCHAR(30) NOT NULL,
	`destination` VARCHAR(30) NOT NULL,
	`time` INT(11) UNSIGNED,
    `cost` DECIMAL(20, 2) UNSIGNED,
	`modified_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE users (
	`id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`firstname` VARCHAR(30) NOT NULL,
	`lastname` VARCHAR(30) NOT NULL,
	`email` VARCHAR(50) NOT NULL,
    `username` VARCHAR(30) NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    `permission` INT(11) UNSIGNED NOT NULL,
	`modified_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO `points` VALUES (NULL, "A", "H", 10, 1, current_timestamp());
INSERT INTO `points` VALUES (NULL, "A", "E", 30, 5, current_timestamp());
INSERT INTO `points` VALUES (NULL, "A", "C", 1, 20, current_timestamp());
INSERT INTO `points` VALUES (NULL, "H", "E", 30, 1, current_timestamp());
INSERT INTO `points` VALUES (NULL, "E", "D", 3, 5, current_timestamp());
INSERT INTO `points` VALUES (NULL, "D", "F", 4, 50, current_timestamp());
INSERT INTO `points` VALUES (NULL, "F", "I", 45, 50, current_timestamp());
INSERT INTO `points` VALUES (NULL, "F", "G", 40, 50, current_timestamp());
INSERT INTO `points` VALUES (NULL, "I", "B", 65, 5, current_timestamp());
INSERT INTO `points` VALUES (NULL, "G", "B", 64, 73, current_timestamp());
INSERT INTO `points` VALUES (NULL, "C", "B", 1, 12, current_timestamp());

INSERT INTO `users` VALUES (NULL, "Aron", "Basco", "aron@xpto.com", "aron", "312433c28349f63c4f387953ff337046e794bea0f9b9ebfcb08e90046ded9c76", 255, current_timestamp());
