<?php

/**
 * Class to build and manage delivery points
 * Parameter must contain the following data
 *  - source, destination, time, cost
 * Map in format of array
 * [
 *      source => [
 *          destination => [
 *              cost => decimal_cost,
 *              time => int_time
 *          ]
 *      ]
 * ]
 */

class Map{
    private $map = Array();

    /**
     * Construct method
     * @param Array|PDO $data Iterable data to be built
     */
    public function __construct($data){
        foreach($data as $row){
            $this->map[$row["source"]][$row["destination"]] = [
                "time" => (int) $row["time"],
                "cost" => (float) $row["cost"],
            ];
        }
    }

    /**
     * Find shortest path using modified djikstra algorithm
     * Constraints: There should always be an intermediary node for source and destination
     * Factors: Can be select from time, cost or all factors
     * @param string $source Source Point
     * @param string $dest Destination Point
     * @param string $factor Factor to be considered in getting the path
     * @param boolean $verbose For debugging purpose
     * @return array Path
     */
    function findShortest($source, $dest, $factor = "all", $verbose = true){
        $vertex = $this->map;

        $factor = in_array($factor, ["time", "cost", "all"]) ? $factor : "time";
        $found = false;
    
        $queue = [ $source => 0 ];
        $path = [$source => "-"];
        $visited = [];
    
        while(!empty($queue) && !$found){
            foreach($queue as $qNode => $weight){
                $current = $qNode;
    
                if(isset($vertex[$current])){
                    foreach($vertex[$current] as $node => $info){
                        if( !in_array($node, $visited) && ($node != $dest || $current != $source) ){
                            if($factor == "all"){
                                $newWeight = $weight + $info["time"] + $info["cost"];
                            }
                            else{
                                $newWeight = $weight + $info[$factor];
                            }
    
                            if( isset($queue[$node]) ){
                                if($newWeight < $queue[$node]){
                                    $queue[$node] = $newWeight;
                                    $path[$node] = $current;
                                }
                            }
                            else{
                                $queue[$node] = $newWeight;
                                $path[$node] = $current;
                            }
                            // echo $current . " => " . $node . ": " . $queue[$node] . " $source $dest " . "\n";
        
                            if($node == $dest){
                                $found = true;
                            }
                        }
                    }
        
                    // echo "Sort: ". implode(array_keys($queue)) ."\n";
                    // echo "Visited: ". implode($visited) ."\n";
                    // echo "\n\n";
                }
        
                // unset visited
                unset($queue[$qNode]);
                $visited[] = $qNode;
    
                // sort queue
                asort($queue);
        
                // restart queue
                break;
            }
    
            if($found){
                break;
            }
        }
    
        return $this->getPath($path, $source, $dest);
    }

    /**
     * Trace route. Specifically used for getting the path from source to destination
     * @param array $routes Set of possible routes
     * @param string $source Source Point
     * @param string $dest Destination Point
     * @return array Traced Path
     */
    private function getPath($routes, $source, $dest){
        $current = $dest;
        $path = [$dest];
    
        while($current != $source){
            // no possible path
            if(!isset($routes[$current])){
                return false;
            }
            array_unshift($path, $routes[$current]);
            $current = $routes[$current];
        }
    
        return $path;
    }

    /**
     * Get the details of each route
     * @param array $route Set of routes to be checked
     * @return array Route summary
     */
    public function getDetails($route){
        $vertex = $this->map;

        $totalTime = 0;
        $totalCost = 0;
        for($i = 0; $i < count($route); $i++){
            if($i + 1 < count($route)){
                $totalTime += $vertex[$route[$i]][$route[$i+1]]["time"];
                $totalCost += $vertex[$route[$i]][$route[$i+1]]["cost"];
            }
        }
    
        return [
            "totalTime" => $totalTime,
            "totalCost" => $totalCost
        ];
    }

    /**
     * Get the generated map from db
     * @return array $this->map
     */
    public function getMap(){
        return $this->map;
    }
}