<?php

/**
 * Access Object
 * Contains authorization functions and access details
 */
class Access {
    const READ = 128;
    const WRITE = 64;
    const DELETE  = 32;
    const PRINT = 16;
    const UPLOAD = 8;
    const EXEC = 4;
    const AUDIT = 2;
    const SUPER = 1;

    protected $token = "";
    private $db = '';
    private $username = "";
    private $password = "";

    /**
     * Construct method
     * @param DB $db Database instance
     * @param string $username User registered Username
     * @param string $password User Password
     * @param string $token Token String
     */
    public function __construct(DB $db, $username = "", $password = "", $token = ""){
        $this->db = $db;
        $this->token = $token;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Authorization Method
     * @param Request $request User request data to be authorized with
     * @return mixed True if authorized, False otherwise. Return -1 for DB PDO Exception
     */
    public function auth(Request $request){
        // authorize request for post, put and delete request
        if($request->type != "get"){
            if($this->username == "" || $this->password == ""){
                return false;
            }

            $user = $this->username;
            $pword = hash("sha256", $this->password);
            $pdo = $this->db->getPDO();

            try{
                $statement = $pdo->prepare("SELECT `permission` FROM `users` WHERE `username` = :username AND `password` = :password");
                $statement->execute([
                    ":username" => $user,
                    ":password" => $pword
                ]);
                $result = $statement->fetchAll();
            }
            catch(PDOException $e){
                return -1;
            }

            if($result){
                $permission = $result[0]['permission'];

                // user must have a super access
                return $permission & self::SUPER;
            }
            else{
                return false;
            }
        }
        else{
            return true;
        }
    }
}