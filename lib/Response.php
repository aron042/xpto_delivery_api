<?php

/**
 * Response Object to be returned
 */
class Response{
    private $type = "";
    private $data;
    private $response;


    /**
     * Construct method
     * @param Request $request The input
     * @param array The response data to be encoded
     */
    public function __construct(Request $request, $data = []){
        $this->data = $data;
        $this->setResponseType($request->responseType);
        $this->encodeResponse();

    }

    /**
     * Set the reponse type for request
     */
    private function setResponseType($type){
        $this->type = $type;
    }

    /**
     * Encodes the reponse to be returned
     */
    private function encodeResponse(){
        switch($this->type){
            case "json":
                $this->response = json_encode($this->data);
                break;
            default:
                $this->response = $this->data;
                break;
        }
    }

    /**
     * Returns the generated response
     * @return string The final encoded response
     */
    public function getResponse(){
        return $this->response;
    }
}