<?php

/**
 * Request Object to be processed
 */
class Request{
    public $responseType = "json";
    public $data;
    public $type = "";
    private $request;

    /**
     * Construct method
     * @param array $data The input
     * @param string $type Request type
     */
    public function __construct($data, $type){
        // get request type
        if($type == "get"){
            $this->type = "get";
        }
        else if($type == "post"){
            if(isset($data['_method'])){
                if($data['_method'] == "put"){
                    $this->type = "put";
                }
                if($data['_method'] == "delete"){
                    $this->type = "delete";
                }
            }
            else{
                $this->type = "post";
            }
        }

        if($this->request = $data){
            $this->generateRequest();
        }
    }

    /**
     * Generate request function
     * Sets the overall api request
     */
    private function generateRequest(){
        if($this->type == "get"){
            if(isset($this->request["source"]) && isset($this->request["destination"])){
                $this->data = [
                    "request" => "findShortest",
                    "source" => $this->request["source"],
                    "destination" => $this->request["destination"]
                ];
            }
        }
        else if($this->type == "post"){
            $this->data = [
                "request" => "addNode",
                "mode" => $this->request["direction"],
                "source" => $this->request["source"],
                "destination" => $this->request["destination"],
                "time" => $this->request["time"],
                "cost" => $this->request["cost"]
            ];
        }
        else if($this->type == "put"){
            $this->data = [
                "request" => "updateSourceNode",
                "id" => $this->request["id"],
                "newSource" => $this->request["source"],
                "newDestination" => $this->request["destination"],
                "newTime" => $this->request["time"],
                "newCost" => $this->request["cost"]
            ];
        }
        else if($this->type == "delete"){
            $this->data = [
                "request" => "deleteNode",
                "node" => $this->request["node"]
            ];
        }
    }
}