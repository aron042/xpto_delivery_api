<?php

/**
 * MySql DB Object
 * Contains mysql query related methods
 */
class DB {
    private $conn;
    private $query;
    private $statement;
    public $error;

    public function __construct($server = "localhost", $db = "xpto_delivery", $user = "root", $password = "root"){
        try{
            $this->conn = new PDO(
                "mysql:host=$server;dbname=$db",
                $user,
                $password
            );
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e){
            $this->error = $e->getMessage();
        }
    }

    public function getPDO(){
        return $this->conn;
    }

    public function makeQuery($queryString){
        return $this->conn->query($queryString);
    }

    public function execQuery($queryString){
        return $this->conn->exec($queryString);
    }

    public function exec(){

    }

    public function select($table, $control){

    }

    public function insert($table, $values){

    }

    public function update($table, $control, $data){

    }

    public function delete($table, $control){

    }
}