CREATE DATABASE IF NOT EXISTS `xpto_delivery`;
USE `xpto_delivery`;

CREATE TABLE points (
	`id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`source` VARCHAR(30) NOT NULL,
	`destination` VARCHAR(30) NOT NULL,
	`time` INT(11) UNSIGNED,
    `cost` DECIMAL(20, 2) UNSIGNED,
	`modified_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE users (
	`id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`firstname` VARCHAR(30) NOT NULL,
	`lastname` VARCHAR(30) NOT NULL,
	`email` VARCHAR(50) NOT NULL,
    `username` VARCHAR(30) NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    `permission` INT(11) UNSIGNED NOT NULL,
	`modified_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO `points` VALUES (NULL, "S", "C", 3, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "S", "B", 2, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "S", "A", 7, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "B", "S", 2, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "B", "A", 3, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "B", "D", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "B", "H", 1, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "A", "S", 7, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "A", "B", 3, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "A", "D", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "C", "S", 3, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "C", "L", 2, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "D", "A", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "D", "B", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "D", "F", 5, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "F", "D", 5, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "F", "H", 3, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "H", "B", 1, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "H", "F", 3, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "H", "G", 2, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "G", "H", 2, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "G", "E", 2, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "L", "C", 2, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "L", "I", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "L", "J", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "I", "L", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "I", "J", 6, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "I", "K", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "J", "L", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "J", "I", 6, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "J", "K", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "K", "I", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "K", "J", 4, 0, current_timestamp());
INSERT INTO `points` VALUES (NULL, "K", "E", 4, 0, current_timestamp());

INSERT INTO `users` VALUES (NULL, "Aron", "Basco", "aron@xpto.com", "aron", "312433c28349f63c4f387953ff337046e794bea0f9b9ebfcb08e90046ded9c76", 255, current_timestamp());