<?php

header("Content-Type:application/json");
require_once("include.php");

$request = false;
if(!empty($_GET)){
    $request = new Request($_GET, "get");
}
else if(!empty($_POST)){
    $request = new Request($_POST, "post");
}

$responseData = [
    "success" => 0,
    "message" => "Undefined Request. Please check your input."
];
if($request && $request->data){

    try{
        $db = new DB($config_db_server, $config_db_schema, $config_db_user, $config_db_password);

        // user authorization
        $username = isset($_REQUEST['username']) ? $_REQUEST['username'] : "";
        $password = isset($_REQUEST['password']) ? $_REQUEST['password'] : "";
        $access = new Access($db, $username, $password);

        if(!$authRes = $access->auth($request)){
            $responseData = [
                "success" => 0,
                "message" => "Unauthorized Access. You are not allowed to make the request."
            ];
        
            $response = new Response($request, $responseData);
            echo $response->getResponse();
            die;
        }
        if($authRes === -1){
            $responseData = [
                "success" => 0,
                "message" => "Database Exception. Unable to fetch user credentials."
            ];
        
            $response = new Response($request, $responseData);
            echo $response->getResponse();
            die;
        }

        // process request
        switch($request->data["request"]){
            // find shortest path request
            case "findShortest":
                $query = "SELECT * FROM points";
                $data = $db->makeQuery($query);
        
                if($data){
                    $map = new Map($data);
    
                    $source = $request->data["source"];
                    $dest = $request->data["destination"];

                    $path = $map->findShortest($source, $dest);
                    $responseData = $map->getDetails($path);
                    $responseData["success"] = empty($path) ? 0 : 1;
                    $responseData["message"] = empty($path) ? "Failed. Couldn't find route from source to destination." : "Success. Path found.";
                    $responseData["string_path"] = implode(" -> ", $path);
                    $responseData["array_path"] = $path;
                }
                break;
            // add node
            case "addNode":
                $pdo = $db->getPDO();
                $statement = $pdo->prepare("INSERT INTO `points` (`source`, `destination`, `time`, `cost`) VALUES (:source, :destination, :time, :cost)");

                $responseData = [];
                try{
                    $source = $request->data["source"];
                    $destination = $request->data["destination"];

                    // check duplicate
                    $checkStatement = $pdo->prepare("SELECT * FROM `points` WHERE `source` = ? AND `destination` = ? ");
                    $checkStatement->execute([$source, $destination]);
                    $exists = count($checkStatement->fetchAll());
                    $checkStatement->closeCursor();

                    if(!$exists){
                        $pdo->beginTransaction();

                        $statement->execute([
                            ":source" => $request->data["source"],
                            ":destination" => $request->data["destination"],
                            ":time" => (int) $request->data["time"],
                            ":cost" => (float) $request->data["cost"]
                        ]);
                        $responseData["success"] = 1;

                        // for bidirectional vertex
                        if($request->data["mode"] == 2){
                            $checkStatement->execute([$destination, $source]);
                            $exists = count($checkStatement->fetchAll());
                            $checkStatement->closeCursor();
                            if(!$exists){
                                $statement->execute([
                                    ":source" => $request->data["destination"],
                                    ":destination" => $request->data["source"],
                                    ":time" => (int) $request->data["time"],
                                    ":cost" => (float) $request->data["cost"]
                                ]);
                                $responseData["message"] = "Success. Nodes inserted";
                            }
                            else{
                                $responseData["message"] = "Success. Duplicate for other direction";
                            }
                        }
                        else{
                            $responseData["message"] = "Success. Node inserted";
                        }

                        $pdo->commit();
                    }
                    else{
                        $responseData = [
                            "success" => 0,
                            "message" => "Duplicate Entry. Failed to insert node."
                        ];
                    }
                }
                catch (PDOException $e){
                    $responseData = [
                        "success" => 0,
                        "message" => "Database Exception. Failed to insert node."
                    ];
                }

                break;
            // update node
            case "updateSourceNode":
                $pdo = $db->getPDO();
                $statement = $pdo->prepare("UPDATE `points` SET `source` = :source, `destination` = :destination, `time` = :time, `cost` = :cost WHERE `id` = :id");

                $responseData = [];
                try{
                    $pdo->beginTransaction();

                    $statement->execute([
                        ":source" => $request->data["newSource"],
                        ":destination" => $request->data["newDestination"],
                        ":time" => (int) $request->data["newTime"],
                        ":cost" => (float) $request->data["newCost"],
                        ":id" => $request->data["id"]
                    ]);
                    $responseData = [
                        "success" => 1,
                        "message" => "Success. Node updated"
                    ];

                    $pdo->commit();
                }
                catch (PDOException $e){
                    $responseData = [
                        "success" => 0,
                        "message" => "Database Exception. Failed to update node."
                    ];
                }

                break;
            // delete node
            case "deleteNode":
                $pdo = $db->getPDO();
                $statement = $pdo->prepare("DELETE FROM points WHERE `source` = :node OR `destination` = :node");

                $responseData = [];
                try{
                    $pdo->beginTransaction();

                    $statement->execute([
                        ":node" => $request->data["node"]
                    ]);
                    $responseData = [
                        "success" => 1,
                        "message" => "Success. Nodes deleted"
                    ];

                    $pdo->commit();
                }
                catch (PDOException $e){
                    $responseData = [
                        "success" => 0,
                        "message" => "Database Exception. Failed to delete node."
                    ];
                }

                break;
            default: 
                $responseData = [
                    "success" => 0,
                    "message" => "Undefined Request. Please check your input."
                ];

                break;
        }
    } catch(Exception $e){
        $responseData = [
            "success" => 0,
            "message" => "Exception Caught. Failed to process request."
        ];
    }

}

$response = new Response($request, $responseData);
echo $response->getResponse();


$vertex = [
    "S" => [
        "C" => [
            "time" => 3,
            "cost" => 1
        ],
        "B" => [
            "time"  => 2,
            "cost"  => 5
        ],
        "A" =>[
            "time"  => 7,
            "cost"  => 20
        ]
    ],
    "B" => [
        "S" => [
            "time" => 2,
            "cost" => 1
        ],
        "A" => [
            "time"  => 3,
            "cost"  => 5
        ],
        "D" =>[
            "time"  => 4,
            "cost"  => 20
        ],
        "H" =>[
            "time"  => 1,
            "cost"  => 20
        ]
    ],
    "A" => [
        "S" => [
            "time" => 7,
            "cost" => 1
        ],
        "B" => [
            "time"  => 3,
            "cost"  => 5
        ],
        "D" =>[
            "time"  => 4,
            "cost"  => 20
        ]
    ],
    "C" => [
        "S" => [
            "time" => 3,
            "cost" => 1
        ],
        "L" => [
            "time"  => 2,
            "cost"  => 5
        ]
    ],
    "D" => [
        "A" => [
            "time" => 4,
            "cost" => 1
        ],
        "B" => [
            "time"  => 4,
            "cost"  => 5
        ],
        "F" => [
            "time"  => 5,
            "cost"  => 5
        ]
    ],
    "F" => [
        "D" => [
            "time" => 5,
            "cost" => 1
        ],
        "H" => [
            "time"  => 3,
            "cost"  => 5
        ]
    ],
    "H" => [
        "B" => [
            "time" => 1,
            "cost" => 1
        ],
        "F" => [
            "time"  => 3,
            "cost"  => 5
        ],
        "G" => [
            "time"  => 2,
            "cost"  => 5
        ]
    ],
    "G" => [
        "H" => [
            "time" => 2,
            "cost" => 1
        ],
        "E" => [
            "time"  => 2,
            "cost"  => 5
        ]
    ],
    "L" => [
        "C" => [
            "time" => 2,
            "cost" => 1
        ],
        "I" => [
            "time"  => 4,
            "cost"  => 5
        ],
        "J" => [
            "time"  => 4,
            "cost"  => 5
        ]
    ],
    "I" => [
        "L" => [
            "time" => 4,
            "cost" => 1
        ],
        "J" => [
            "time"  => 6,
            "cost"  => 5
        ],
        "K" => [
            "time"  => 4,
            "cost"  => 5
        ]
    ],
    "J" => [
        "L" => [
            "time" => 4,
            "cost" => 1
        ],
        "I" => [
            "time"  => 6,
            "cost"  => 5
        ],
        "K" => [
            "time"  => 4,
            "cost"  => 5
        ]
    ],
    "K" => [
        "I" => [
            "time" => 4,
            "cost" => 1
        ],
        "J" => [
            "time"  => 4,
            "cost"  => 5
        ],
        "E" => [
            "time"  => 4,
            "cost"  => 5
        ]
    ]
];

$vertex1 = [
    "A" => [
        "H" => [
            "time" => 10,
            "cost" => 1
        ],
        "E" => [
            "time"  => 30,
            "cost"  => 5
        ],
        "C" =>[
            "time"  => 1,
            "cost"  => 20
        ]
    ],
    "H" => [
        "E" => [
            "time"  => 30,
            "cost"  => 1
        ]
    ],
    "E" => [
        "D" => [
            "time"  => 3,
            "cost"  => 5
        ]
    ],
    "D" => [
        "F" => [
            "time"  => 4,
            "cost"  => 50
        ]
    ],
    "F" => [
        "I" => [
            "time"  => 45,
            "cost"  => 50
        ],
        "G" => [
            "time"  => 40,
            "cost"  => 50
        ]
    ],
    "I" => [
        "B" => [
            "time"  => 65,
            "cost"  => 5
        ]
    ],
    "G" => [
        "B" => [
            "time"  => 64,
            "cost"  => 73
        ]
    ],
    "C" => [
        "B" => [
            "time"  => 1,
            "cost"  => 12
        ]
    ]
];

?>