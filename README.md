﻿# XPTO Delivery API

A simple Delivery API for getting shortest path from source point to destination. This uses a modified ***Dijkstra Algorithm*** to be able to adjust from the factors given ( or to be implemented).


## Tech Used

The API is built in PHP7 and uses PDO for MySql database interactions.

## Configurations

Database configurations can be found in `config.php` file. Modify the credentials here to adjust to your local machines.

Local libraries can be found in `lib` directory.

 - **Access**: for managing user access and authorization
 - **Database**: for Mysql DB interactions
 - **Map**: contains implemented methods for getting shortest path
 - **Request**: finalizes the request
 - **Response**: encodes response to be thrown

## Usage and Examples

You may **Postman** to do testing or any other API Development platform for your convenience.

### **Getting Shortest Path**
Use a `GET` verb to get the shortest path for source and destination by supplying values to `source` and `destination` parameter. For instance:

	http://localhost/xpto_delivery_api/api.php?source=A&destination=B

In this case, using the sample 1 at the buttom, the result path would be **`A -> C -> B`**. Note that it won't find a path for direct route, for instance source **A** to **C**, this won't have a path.

### **Adding, Updating and Deleting Node**
To add, update, and delete, use http verbs `post`, `put`, and `delete` respectively. 

#### *Adding Sample*
The following will add a route from J to K and vice versa with 15 as ***time*** and 11 as ***cost***. Note that this is a two way route and is made possible by giving a value of 2 to ***direction***.

    curl --location --request POST 'http://localhost/xpto_delivery_api/api.php' \
    --form 'source=J' \
    --form 'destination=K' \
    --form 'time=15' \
    --form 'cost=11' \
    --form 'direction=2' \
    --form 'username=aron' \
    --form 'password=asdf1234'

#### *Updating Sample*
To update source, follow the example bellow. Source **A** will have a new destination **Z** if it's a new destination then updates Row 20 with the other information.

    curl --location --request POST 'http://localhost/xpto_delivery_api/api.php' \
    --form 'source=A' \
    --form 'destination=Z' \
    --form 'time=4' \
    --form 'cost=3' \
    --form '_method=put' \
    --form 'id=20' \
    --form 'username=aron' \
    --form 'password=asdf1234'

#### *Deleting Sample*
For deleting entry, supply a node to be deleted. In the following, Source and Destination **J** will be deleted.

    curl --location --request POST 'http://localhost/xpto_delivery_api/api.php' \
    --form '_method=delete' \
    --form 'node=J' \
    --form 'username=aron' \
    --form 'password=asdf1234'

### **Access Restriction**
There are 6 Access Rights and to perform the CRUD functionality, a user should have a Super Access right which is determined from the permission column of users table. Computation is:

	super_access = permission & 1

Above examples supplies a `username` and `password` data for authorization and from the sample solution below, user `aron` has a permission of 255 and has a super access.

## Sample Solution

Download and import `xpto_delivery - 1.sql` for the following graph.
![](sample_graph_1.png)

Download and import `xpto_delivery - 2.sql` for another sample graph.

*Make sure to delete the old schema before importing new one.*